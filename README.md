# Ubuntu container from scratch

### Running instructions
- Copy the ubuntu filesystem. I copied it from a docker contaner using ```docker cp``` command. <br>
- Run directly <br>```sudo go run container.go run <command>```
- build and run <br> ```go build container.go && sudo ./container run <command>```
