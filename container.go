package main

import (
	"fmt"
	"os"
	"os/exec"
	"syscall"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func run() {
	fmt.Printf("run : %v\n", os.Args[2:])
	cmd := exec.Command("/proc/self/exe", append([]string{"child"}, os.Args[2:]...)...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	cmd.SysProcAttr = &syscall.SysProcAttr {
		Cloneflags : syscall.CLONE_NEWUTS | syscall.CLONE_NEWPID,
	}

	check(cmd.Run())
}

func child() {
	fmt.Printf("running %v as PID %d\n", os.Args[2:], os.Getpid())
	cmd := exec.Command(os.Args[2], os.Args[3:]...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	check(syscall.Chroot("./file-system"))
	check(os.Chdir("/"))
	check(syscall.Mount("proc", "proc", "proc", 0, ""))
	check(cmd.Run())
}



func main() {
	switch os.Args[1] {
	case "run":
		run()
	case "child":
		child()
	case "help":
		fmt.Println(" - run : run a new container")
	default:
		panic("wrong command\n do help for help.")
	}
}

